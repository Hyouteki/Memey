package com.hyouteki.projects.memey.comms

interface FragMainComms {
    fun showProgressBar() {}
    fun dismissProgressBar() {}
}